const btn = document.querySelector("button");
const select = document.querySelector("select");
const output = document.querySelector("#output");
const input = document.getElementById("input");
const url = "https://api.nbp.pl/api/exchangerates/tables/a/";

fetch(url)
  .then((response) => response.json())
  .then((data) =>
    Object.entries(data[0].rates).forEach((ele, value) => {
      if (
        ele[1].code === "USD" ||
        ele[1].code === "EUR" ||
        ele[1].code === "CHF"
      ) {
        const option = document.createElement("option");
        option.innerHTML = ele[1].code;
        option.id = value;
        option.value = ele[1].mid;
        select.appendChild(option);
      }
    })
  )
  .catch((err) => console.error(err));

btn.addEventListener("click", function exchange() {
  const options = select.options;
  output.innerHTML = `Exchanging ${input.value} ${
    select[options.selectedIndex].textContent
  } at the rate of ${parseFloat(select.value).toFixed(2)} PLN for 1.00 ${
    select[options.selectedIndex].textContent
  } will get you ${(input.value * select.value).toFixed(2)} PLN `;
});
